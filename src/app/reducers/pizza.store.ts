import {createAction, createFeatureSelector, createReducer, createSelector, on,} from '@ngrx/store';

// actions
// TODO createAction(...)

// reducer
interface PizzaState {
  hasPepperoni: boolean;
  hasSausage: boolean;
}

const initialState: PizzaState = {
  hasPepperoni: false,
  hasSausage: false,
};

export const pizzaReducer = createReducer<PizzaState>(
  initialState,
  // TODO on(...)
);

// selectors
export const pizzaFeatureKey = 'pizza';

const CHEESE_PIZZA_IMAGE = 'assets/cheese_pizza.jpeg';
const PEPPERONI_PIZZA_IMAGE = 'assets/pepperoni_pizza.jpeg';
const SAUSAGE_PIZZA_IMAGE = 'assets/sausage_pizza.jpeg';
const PEPPERONI_AND_SAUSAGE_PIZZA_IMAGE = 'assets/sausage_and_pepperoni_pizza.jpeg';

function determineImageUrl({hasPepperoni, hasSausage}: PizzaState): string {
  if (hasPepperoni && hasSausage) {
    return PEPPERONI_AND_SAUSAGE_PIZZA_IMAGE;
  } else if (hasPepperoni) {
    return PEPPERONI_PIZZA_IMAGE;
  } else if (hasSausage) {
    return SAUSAGE_PIZZA_IMAGE;
  } else {
    return CHEESE_PIZZA_IMAGE;
  }
}

const STARTING_PRICE = 12;
const PEPPERONI_PRICE = 3;
const SAUSAGE_PRICE = 2.5;

function determinePrice({hasPepperoni, hasSausage}: PizzaState): number {
  return (
    STARTING_PRICE +
    (hasPepperoni ? PEPPERONI_PRICE : 0) +
    (hasSausage ? SAUSAGE_PRICE : 0)
  );
}

const pizzaFeatureSelector = createFeatureSelector<PizzaState>(
  pizzaFeatureKey
);

// TODO createSelector(...)
