import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {EMPTY, Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  imgUrl$: Observable<string> = EMPTY; // TODO = this.store.select(...);
  price$: Observable<number> = EMPTY; // TODO = this.store.select(...);
  hasPepperoni$: Observable<boolean> = EMPTY; // TODO = this.store.select(...);
  hasSausage$: Observable<boolean> = EMPTY; // TODO = this.store.select(...);

  constructor(private store: Store) {
  }

  ngOnInit(): void {
  }

  pepperoniClick(hasPepperoni: boolean): void {
    // TODO this.store.dispatch(...);
  }

  sausageClick(hasSausage: boolean): void {
    // TODO this.store.dispatch(...);
  }
}
